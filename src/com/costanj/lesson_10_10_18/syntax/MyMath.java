package com.costanj.lesson_10_10_18.syntax;

public class MyMath {
    public int checkValues(int c, int d, int... a) {
        int counter = 0;
        for(int num : a) {
            if(c <= num && d >= num) {
                counter++;
            }
        }
        return counter;
    }
}
